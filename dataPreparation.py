import gensim
from gensim.models import KeyedVectors
from datetime import datetime
import numpy as np
import tensorflow as tf
import random
import pandas as pd
import re
random.seed(123)
import pickle
from gensim.models import Word2Vec

trainPercent = 0.8
wordEmbeddingSize = 200

def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"\.\.*", ".", string)
    string = re.sub(r"\?\?*", ".", string)
    string = re.sub(r"[^A-Za-z0-9.,()!?\'\`<>\/]", " ", string)
    string = re.sub(r"\'s", "", string)
    string = re.sub(r"\'ve", "", string)
    string = re.sub(r"can\'t", "can not", string)
    string = re.sub(r"isn\'t", "is not", string)
    string = re.sub(r"haven\'t", "have not", string)
    string = re.sub(r"\'re", " are", string)
    string = re.sub(r"\'d", "", string)
    string = re.sub(r"\'ll", "", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"\.", " . ", string)
    string = re.sub(r"/", " / ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"'", "", string)
    string = re.sub(r"\(", "", string)
    string = re.sub(r"\)", "", string)
    string = re.sub(r"\?", "?", string)
    string = re.sub(r"\s{2,}", " ", string)
    string = re.sub(r"<br / >", "", string)
    string = re.sub(r" [A-Za-z] ", " ", string)
    return string.strip().lower()

stopwords = ['hi','ha','haan','aur','ok','hai','ji','jo','na','hu','ya','no','yes','yaar','hello','yeh','sure','ek','bas','woh','wo','hoti','hota','ho','koi','bhi','sir','mam','ki','ko','kar','rahe','raha','rahi','hua','ka','se','hoon','ke','tha','thi','to','is','an','the','was','so','much','for','in','on','were','have','has','had','having','will','would','shall','should','had','yeah','do','does','of','as','am','be','been','being']
def remove_stopwords(stopwords,doc):
    doc = clean_str(doc)
    tokenizedDoc = doc.split()
    preparedDoc = [tok for tok in tokenizedDoc if len(tok)>1 or tok == '.' or tok == '.']
    preparedDoc = ' '.join(preparedDoc)
    return preparedDoc

def load_data():
    d = pd.read_csv('axiom_survey.csv')
    print('shape of the data: {}'.format(d.shape))
    return d

def sentence_cleaning(dataframe):
    def feedback_cleaning(feedback):
        cleaned_feedback = map(lambda x:remove_stopwords(stopwords,x), feedback)
        #hash_removed = map(lambda x: re.sub(r'#', r' ', str(x)), lowered_feedback)
        #remove_symbols = map(lambda x: re.sub('_x000d_'.lower(), ' ', x), hash_removed)
        #special_chars_removed = map(lambda x: re.sub(r'[^A-Za-z0-9 ]', r'', x), remove_symbols)
        return cleaned_feedback
    
    dataframe['cleaned_feedback'] = list(feedback_cleaning(dataframe['Feedback'].values))
    dataframe.drop('Feedback', axis=1, inplace=True)
    return dataframe

def getVocab(data):
    sensList = data['cleaned_feedback'].tolist()
    vocabDict = {}
    for sen in sensList:
        spSen = sen.split()
        for word in spSen:
            if word not in vocabDict:
                vocabDict[word] = len(vocabDict)
    vocabDict['<padWord>'] = len(vocabDict)
    vocabDict['<UNK>'] = len(vocabDict)
    return vocabDict

axiom_data = load_data()
axiom_data = axiom_data.rename(index=str, columns={"Dept 2": "Dept_2", "Sub Dept 2": "Sub_Dept_2", "Survey Name 2" : "Survey_Name_2"})
axiom_data.loc[axiom_data.Dept_2 == 'Reverse logistics', 'Dept_2'] = 'Reverse Logistics'
axiom_data.loc[axiom_data.Sub_Dept_2 == 'Pickup / Exchange / refund time', 'Sub_Dept_2'] = 'Pickup/exchange/refund time'
axiom_data = sentence_cleaning(axiom_data)
axiom_data = axiom_data[['cleaned_feedback', 'Dept_2', 'Sub_Dept_2','Sentiment']]
axiom_data = axiom_data.sample(frac=1, replace=False)
print("shape before deleting duplicates : ",axiom_data.shape)
axiom_data = axiom_data.drop_duplicates(subset=['cleaned_feedback','Dept_2','Sub_Dept_2']) #
print("shape after deleting duplicates : ",axiom_data.shape)
axiom_data = axiom_data.drop_duplicates(subset=['cleaned_feedback','Sub_Dept_2'])
print("shape after deleting duplicates 2nd time : ",axiom_data.shape)

data_dict = {}
for index, row in axiom_data.iterrows():
    curInput = row['cleaned_feedback'] #row['cleaned_feedback'] #
    curL1Label = row['Dept_2']
    curL2Label = row['Sub_Dept_2']
    if curInput not in data_dict:
        data_dict[curInput] = []
    data_dict[curInput].append([curL1Label,curL2Label])

data_dict_df = pd.DataFrame(columns=['input_sen','labels'])
senIndex = 0
for key,val in data_dict.items():
    data_dict_df.loc[senIndex] = [key,val]
    senIndex += 1
    if senIndex % 10000 == 0:
        print(senIndex,"...",str(datetime.now()))

print(data_dict_df.shape)
data_dict_df.to_csv("axiom_multiLabel_data_withSW.csv")
