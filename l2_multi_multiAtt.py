
# coding: utf-8

# In[1]:


import gensim
from gensim.models import KeyedVectors
from datetime import datetime
import numpy as np
import random
import pandas as pd
import re
import tensorflow as tf
import tensorflow_hub as hub
import tensorflow.contrib.layers as layers
random.seed(123)
import pickle
import os
import sklearn
import ast
from sklearn.metrics import confusion_matrix

os.environ["CUDA_VISIBLE_DEVICES"]="0"
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

wordEmbeddingSize = 200

#sp_test_df = pd.DataFrame(columns=['input_sen','labels'])
#sp_test_df.loc[0] = ['happy wid product size everything like mytra',"[['Category', 'Product quality'], ['Catalog', 'Size chart']]"]
#sp_test_df.loc[1] = ['issue with product but more with delivery 10 days thats too also tracking myantra not good good days tracking info available also ipad app runs very slow',"[['Delivery', 'Delivery communications / updates'], ['App interface+Website', 'App interface'], ['Delivery', 'Delivery speed']]"]

with open('level2Labels.pkl', 'rb') as ff:
    level2Labels = pickle.load(ff)
print("len(level2Labels) : ",len(level2Labels))
L2LabelsDict = {}
for value2 in level2Labels:
    L2LabelsDict[value2] = len(L2LabelsDict)
nofClassesInL2 = len(L2LabelsDict)
revLabelsDict = {}
for key, val in L2LabelsDict.items():
    revLabelsDict[val] = key

train_df = pd.read_csv("train_axiom_multiLabel_data_withSW.csv")
test_df = pd.read_csv("test_axiom_multiLabel_data_withSW.csv")
print("train_df.shape :",train_df.shape)
print("test_df.shape :",test_df.shape)
#
# In[28]:
def getVocab(data):
    sensList = data['input_sen'].tolist()
    vocabDict = {}
    for sen in sensList:
        if isinstance(sen, str)==False:
            continue
        spSen = sen.split()
        for word in spSen:
            if word not in vocabDict:
                vocabDict[word] = len(vocabDict)
    vocabDict['<padWord>'] = len(vocabDict)
    vocabDict['<UNK>'] = len(vocabDict)
    return vocabDict
vocabDict = getVocab(train_df)
with open('AxiomWVsDict.pkl', 'rb') as AxiomWVsDictFile:
    AxiomWVsDict = pickle.load(AxiomWVsDictFile)
print("vocab len :",len(vocabDict))
print("wvs dict len :",len(AxiomWVsDict))
# In[33]:


def getMaxSenLen(batch_data_df):
    maxSenLenB = 0
    nofSensInBatch = 0
    for index, row in batch_data_df.iterrows():
        curSen = row['input_sen'] ###
        if isinstance(curSen, str)==False:
            print("Not a sentence :",curSen)
            continue
        curSenLen = len(curSen.split())
        maxSenLenB = max(curSenLen,maxSenLenB)
        nofSensInBatch+=1
    return maxSenLenB,nofSensInBatch

def getEncodedData(batch_data_df,vocabDict,L2LabelsDict,AxiomWVsDict):
    #nofSens = batch_data_df.shape[0]
    rawSensList = []
    maxSenLenBatch,nofSensInBatch = getMaxSenLen(batch_data_df)
    lengthsVecArr = np.zeros(shape=[nofSensInBatch])
    #print(maxSenLenBatch)
    encodedData = np.zeros(shape=[nofSensInBatch,maxSenLenBatch])
    nofClassesInL2 = len(L2LabelsDict)
    encodedLabelsL2 = np.zeros(shape=[nofSensInBatch,nofClassesInL2])
    curSenIndex = 0
    for index, row in batch_data_df.iterrows():
        curSen = row['input_sen']
        if isinstance(curSen, str)==False:
             continue
        rawSensList.append(curSen)
        curSpSen = curSen.split()
        curSenLen = len(curSpSen)
        curLabelsList = ast.literal_eval(row['labels']) #ast.literal_eval()
        for label in curLabelsList:
            curL2Label = label[1]
            curLabelIndexL2 = L2LabelsDict[curL2Label]
            encodedLabelsL2[curSenIndex][curLabelIndexL2] = 1
        lengthsVecArr[curSenIndex] = curSenLen
        for i in range(min(curSenLen,maxSenLenBatch)):
            word = curSpSen[i]
            if word in vocabDict:
                if word in AxiomWVsDict:
                    encodedData[curSenIndex][i] = vocabDict[word]
                else:
                    encodedData[curSenIndex][i] = vocabDict['<UNK>']
            else:
                encodedData[curSenIndex][i] = vocabDict['<UNK>']
        while i < maxSenLenBatch:
            encodedData[curSenIndex][i] = vocabDict['<padWord>']
            i+=1
        curSenIndex+=1
    return encodedData,encodedLabelsL2,lengthsVecArr,rawSensList


# In[34]:


vocSize = len(vocabDict)
gsInitVecs = np.array(np.zeros(shape=[vocSize,wordEmbeddingSize]))
for word,index in vocabDict.items():
    if word in AxiomWVsDict:
        gsInitVecs[index,:] = AxiomWVsDict[word]
    else:
        gsInitVecs[index,:] = np.zeros(shape=[wordEmbeddingSize])
    #gsInitVecs[index,:] = np.random.uniform(low=-1.0,high=1.0,size=[wordEmbeddingSize])


# In[35]:


nofClassesInL2 = 41

#Hyperparameters
nofUnitsInH1 = 200
l2_reg_lambda = 0.0001
nofLstmUnits = 400
max_grad_norm = 5.0

#def biAttModel(inputBatchData,inputBatchLabels):
tf.reset_default_graph()
l2_loss = tf.constant(0.0)
dropoutLstmLayer = tf.placeholder(tf.float32)
dropoutPenultimate = tf.placeholder(tf.float32)
X = tf.placeholder(tf.int64, shape=(None, None),name="X")
sensListT = tf.placeholder(shape=(None,),dtype=tf.string, name="sensListT")
(nofInputs,maxSenLen) = tf.unstack(tf.shape(X))
lengthsVec = tf.placeholder(shape=(None,), dtype=tf.int32, name='lengthsVec')
W1 = tf.to_float(tf.Variable(gsInitVecs,name = "noreg1",dtype = tf.float32),name = "noreg")
#W1 = tf.to_float(tf.get_variable(shape=(vocSize,wordEmbeddingSize),name = "noreg1",dtype = tf.float32),name = "noreg")
wemdsX = tf.nn.embedding_lookup(W1,X)
word_level_inputsT = tf.reshape(wemdsX, [nofInputs,maxSenLen,wordEmbeddingSize])

elmo = hub.Module("https://tfhub.dev/google/elmo/2",trainable=True)
elmo_embds = elmo(sensListT,signature="default",as_dict=True)["elmo"]
word_level_inputs = tf.concat([elmo_embds,word_level_inputsT],axis=2)

fcell = tf.contrib.rnn.DropoutWrapper(tf.contrib.rnn.LSTMCell(nofLstmUnits), output_keep_prob=1.0 - dropoutLstmLayer)
bcell = tf.contrib.rnn.DropoutWrapper(tf.contrib.rnn.LSTMCell(nofLstmUnits), output_keep_prob=1.0 - dropoutLstmLayer)
Y2 = tf.placeholder(tf.float32, shape=(None, nofClassesInL2),name="Y2")

#Y2 = tf.placeholder(tf.float32, shape=(None, nofClassesInL2))
outputs, outputStates = tf.nn.bidirectional_dynamic_rnn(fcell,bcell, word_level_inputs, dtype=tf.float32,sequence_length=lengthsVec)

outputsTensor = tf.convert_to_tensor(outputs)
outputsTensor2 = tf.concat([outputsTensor[0], outputsTensor[1]], axis=2)#tf.reduce_max(outputsTensor, axis=0)

multiAttWts = tf.get_variable(name='multiAttWts',shape=[nofClassesInL2,2*nofLstmUnits],initializer=layers.xavier_initializer(),dtype=tf.float32)
vu2 = tf.tensordot(outputsTensor2, multiAttWts, axes=[[2],[1]], name='attention_dot')
alpha1V = tf.transpose(tf.nn.softmax(vu2, name='alphas'),[0,2,1])
token1Vecs = tf.tanh(tf.matmul(a=alpha1V,b=outputsTensor2))
token1Vecs2 = tf.layers.dense(token1Vecs,nofUnitsInH1,activation=tf.nn.relu)
logitsT = tf.squeeze(tf.layers.dense(token1Vecs2,1,activation=None),axis=2)
scores = tf.sigmoid(logitsT,name="scores")

losses = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=Y2, logits=logitsT),axis=1,name="sigmoid_losses")
for tf_var in tf.trainable_variables():
    if not ("noreg" in tf_var.name or "Bias" in tf_var.name):
        l2_loss += tf.nn.l2_loss(tf_var)
loss = tf.reduce_mean(losses) + l2_reg_lambda*l2_loss
tvars = tf.trainable_variables()
grads, global_norm = tf.clip_by_global_norm(tf.gradients(loss, tvars),max_grad_norm)
optimizer = tf.train.AdamOptimizer(0.0005)
global_stepT = tf.Variable(0, name='global_step', trainable=False)
train_op = optimizer.apply_gradients(zip(grads, tvars), name='train_op',global_step=global_stepT)


# In[39]:


def cal_F(recall, precision):
    F = 0.0
    if recall + precision == 0:
        F = 0.0
    else:
        F = (2 * recall * precision) / (recall + precision)
    return F

def printPredictions(batchDataDf,predicted_labelsB,true_labelsB,revLabelsDict):
    sensList = batchDataDf['input_sen'].tolist()
    for ind,sen in enumerate(sensList):
        print("---------------")
        print("sen : ",sen)
        print("correct labels :")
        for true_label in true_labelsB[ind]:
            print("\t",revLabelsDict[true_label])
        print("predicted labels :")
        for pred_label in predicted_labelsB[ind]:
            print("\t",revLabelsDict[pred_label])
            
def cal_metric(predicted_labels, labels):
    label_no_zero = []
    for index, label in enumerate(labels):
        if int(label) == 1:
            label_no_zero.append(index)
    #print("True labels :",label_no_zero)
    count = 0
    for predicted_label in predicted_labels:
        if int(predicted_label) in label_no_zero:
            count += 1
    nofLabsCaptured = 0
    for true_label in label_no_zero:
        if int(true_label) in predicted_labels:
            nofLabsCaptured += 1
    #print("count :",count,"nofLabsCaptured :",nofLabsCaptured)
    recall = count / len(label_no_zero)
    precision = count / len(predicted_labels)
    return recall, precision, label_no_zero,nofLabsCaptured

def findBatchMetrics(batchScores,batchY2):
    threshold = 0.5
    scores = np.ndarray.tolist(batchScores)
    predicted_labelsB = []
    predicted_valuesB = []
    true_labelsB = []
    nofLabsCapturedG = 0
    cur_rec, cur_pre, cur_F = 0.0, 0.0, 0.0
    for scoreTemp in batchScores:
        score = np.ndarray.tolist(scoreTemp)
        count = 0
        index_list = []
        value_list = []
        for index, predict_value in enumerate(score):
            if predict_value > threshold:
                index_list.append(index)
                value_list.append(predict_value)
                count += 1
        if count == 0:
            index_list.append(score.index(max(score)))
            value_list.append(max(score))
        predicted_labelsB.append(index_list)
        predicted_valuesB.append(value_list)
    #print("predicted_labelsB : ",predicted_labelsB)
    
    for index, predicted_labels_list in enumerate(predicted_labelsB):
        rec_inc, pre_inc, true_labels_list,nofLabsCaptured = cal_metric(predicted_labels_list, batchY2[index])
        nofLabsCapturedG += nofLabsCaptured
        cur_rec, cur_pre = cur_rec + rec_inc, cur_pre + pre_inc
        true_labelsB.append(true_labels_list)
    #print("len(batchY2) :",len(batchY2))
    cur_rec = cur_rec / len(batchY2)
    cur_pre = cur_pre / len(batchY2)
    return cur_rec,cur_pre,predicted_labelsB,true_labelsB,nofLabsCapturedG

batchSize = 32
testBatchSize = 32 #256
nofepochs = 8
#train_df = train_df.head(1000)
nofBatches = int(train_df.shape[0]/batchSize)
nofTestbBatches = int(test_df.shape[0]/testBatchSize)
print("nofBatches :",nofBatches)
print("nofTestbBatches :",nofTestbBatches)
maxF1 = 0.0
saver = tf.train.Saver()
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for iterIndex in range(nofepochs):
        train_df = train_df.sample(frac=1, replace=False)
        train_losses = []
        for batchIter in range(nofBatches): #nofBatches
            batchDataDf = train_df[batchIter*batchSize : batchIter*batchSize+batchSize]
            batchX,batchY2,lengthsVecArr,rawSensList = getEncodedData(batchDataDf,vocabDict,L2LabelsDict,AxiomWVsDict)
            _,batchScores,losss = sess.run([train_op,scores,loss], feed_dict={X: batchX, sensListT:rawSensList, Y2:batchY2,lengthsVec:lengthsVecArr, dropoutLstmLayer: 0.4, dropoutPenultimate: 0.3})
            train_losses.append(losss)
            if batchIter % 1000 == 0:
                print(iterIndex, ",", batchIter, ",loss : ", losss,"...........", str(datetime.now()), " ....")
        print("---- Epoch :",iterIndex," -- Train Loss :",float(sum(train_losses))/len(train_losses))        

        test_counter, test_loss, test_rec, test_pre, test_F = 0, 0.0, 0.0, 0.0, 0.0
        nofLabsCapturedG = 0
        nofUniSamplesG = 0
        for testBatchIter in range(nofTestbBatches): #nofTestbBatches
            batchDataDf = test_df[testBatchIter*testBatchSize : testBatchIter*testBatchSize+testBatchSize]
            batchX,batchY2_test,lengthsVecArr,rawSensList = getEncodedData(batchDataDf,vocabDict,L2LabelsDict,AxiomWVsDict)
            scores_batch_test = sess.run(scores, feed_dict={X: batchX, sensListT:rawSensList, Y2:batchY2_test,lengthsVec:lengthsVecArr, dropoutLstmLayer: 0, dropoutPenultimate: 0})
            cur_rec,cur_pre,predicted_labelsB,true_labelsB,nofLabsCapturedB = findBatchMetrics(scores_batch_test,batchY2_test)
            for true_label_list in true_labelsB:
                 nofUniSamplesG += len(true_label_list)
            nofLabsCapturedG += nofLabsCapturedB
            if iterIndex == nofepochs-1 and testBatchIter == nofTestbBatches-1:
                 printPredictions(batchDataDf,predicted_labelsB,true_labelsB,revLabelsDict)
            test_rec, test_pre = test_rec + cur_rec, test_pre + cur_pre
            test_counter += 1
        test_rec = float(test_rec / test_counter)
        test_pre = float(test_pre / test_counter)
        print("nof captured samples :",nofLabsCapturedG,"..nofUniSamples : ",nofUniSamplesG)
        test_F = cal_F(test_rec, test_pre)
        print("iter : ",iterIndex, "Test F1 : ",test_F, "Test Recall : ",test_rec, "Test precision : ",test_pre, "... t_counter :",test_counter, "....",str(datetime.now()) ," ....")
        if test_F > maxF1:
                maxF1 = test_F
                save_path = saver.save(sess, "L2_multiLabel_multiAtt_bestModel/bestModel_biAtt")

