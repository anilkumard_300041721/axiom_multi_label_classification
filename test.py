import gensim
from gensim.models import KeyedVectors
from datetime import datetime
import numpy as np
import random
import pandas as pd
import re
import tensorflow as tf
import tensorflow_hub as hub
import tensorflow.contrib.layers as layers
random.seed(123)
import pickle
import os
import sklearn
import ast

os.environ["CUDA_VISIBLE_DEVICES"]="0"
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

wordEmbeddingSize = 200

with open('level2Labels.pkl', 'rb') as ff:
    level2Labels = pickle.load(ff)
print("len(level2Labels) : ",len(level2Labels))
L2LabelsDict = {}
for value2 in level2Labels:
    L2LabelsDict[value2] = len(L2LabelsDict)
nofClassesInL2 = len(L2LabelsDict)
revLabelsDict = {}
for key, val in L2LabelsDict.items():
    revLabelsDict[val] = key
    
train_df = pd.read_csv("train_axiom_multiLabel_data_withSW.csv")
test_df = pd.read_csv("test_axiom_multiLabel_data_withSW.csv")
print("train_df.shape :",train_df.shape)
print("test_df.shape :",test_df.shape)

def getVocab(data):
    sensList = data['input_sen'].tolist()
    vocabDict = {}
    for sen in sensList:
        if isinstance(sen, str)==False:
            continue
        spSen = sen.split()
        for word in spSen:
            if word not in vocabDict:
                vocabDict[word] = len(vocabDict)
    vocabDict['<padWord>'] = len(vocabDict)
    vocabDict['<UNK>'] = len(vocabDict)
    return vocabDict
vocabDict = getVocab(train_df)
with open('../axiom_seq_toServer_new/AxiomWVsDict.pkl', 'rb') as AxiomWVsDictFile:
    AxiomWVsDict = pickle.load(AxiomWVsDictFile)
print("vocab len :",len(vocabDict))
print("wvs dict len :",len(AxiomWVsDict))

def getMaxSenLen(batch_data_df):
    maxSenLenB = 0
    nofSensInBatch = 0
    for index, row in batch_data_df.iterrows():
        curSen = row['input_sen'] ###
        if isinstance(curSen, str)==False:
            print("Not a sentence :",curSen)
            continue
        curSenLen = len(curSen.split())
        maxSenLenB = max(curSenLen,maxSenLenB)
        nofSensInBatch+=1
    return maxSenLenB,nofSensInBatch

def getEncodedData(batch_data_df,vocabDict,L2LabelsDict,AxiomWVsDict):
    #nofSens = batch_data_df.shape[0]
    rawSensList = []
    maxSenLenBatch,nofSensInBatch = getMaxSenLen(batch_data_df)
    lengthsVecArr = np.zeros(shape=[nofSensInBatch])
    #print(maxSenLenBatch)
    encodedData = np.zeros(shape=[nofSensInBatch,maxSenLenBatch])
    nofClassesInL2 = len(L2LabelsDict)
    encodedLabelsL2 = np.zeros(shape=[nofSensInBatch,nofClassesInL2])
    curSenIndex = 0
    for index, row in batch_data_df.iterrows():
        curSen = row['input_sen']
        if isinstance(curSen, str)==False:
             continue
        rawSensList.append(curSen)
        curSpSen = curSen.split()
        curSenLen = len(curSpSen)
        curLabelsList = ast.literal_eval(row['labels']) #ast.literal_eval()
        for label in curLabelsList:
            curL2Label = label[1]
            curLabelIndexL2 = L2LabelsDict[curL2Label]
            encodedLabelsL2[curSenIndex][curLabelIndexL2] = 1
        lengthsVecArr[curSenIndex] = curSenLen
        for i in range(min(curSenLen,maxSenLenBatch)):
            word = curSpSen[i]
            if word in vocabDict:
                if word in AxiomWVsDict:
                    encodedData[curSenIndex][i] = vocabDict[word]
                else:
                    encodedData[curSenIndex][i] = vocabDict['<UNK>']
            else:
                encodedData[curSenIndex][i] = vocabDict['<UNK>']
        while i < maxSenLenBatch:
            encodedData[curSenIndex][i] = vocabDict['<padWord>']
            i+=1
        curSenIndex+=1
    return encodedData,encodedLabelsL2,lengthsVecArr,rawSensList

def cal_F(recall, precision):
    F = 0.0
    if recall + precision == 0:
        F = 0.0
    else:
        F = (2 * recall * precision) / (recall + precision)
    return F

def printPredictions(batchDataDf,predicted_labelsB,true_labelsB,revLabelsDict):
    sensList = batchDataDf['input_sen'].tolist()
    for ind,sen in enumerate(sensList):
        print("---------------")
        print("sen : ",sen)
        print("correct labels :")
        for true_label in true_labelsB[ind]:
            print("\t",revLabelsDict[true_label])
        print("predicted labels :")
        for pred_label in predicted_labelsB[ind]:
            print("\t",revLabelsDict[pred_label])
            
def cal_metric(predicted_labels, labels):
    label_no_zero = []
    for index, label in enumerate(labels):
        if int(label) == 1:
            label_no_zero.append(index)
    #print("True labels :",label_no_zero)
    count = 0
    for predicted_label in predicted_labels:
        if int(predicted_label) in label_no_zero:
            count += 1
    nofLabsCaptured = 0
    for true_label in label_no_zero:
        if int(true_label) in predicted_labels:
            nofLabsCaptured += 1
    if count != nofLabsCaptured:
        print("Somethig is wrong .... ...............")
        print(sdfsdfdsf)
    #print("count :",count,"nofLabsCaptured :",nofLabsCaptured)
    recall = count / len(label_no_zero)
    precision = count / len(predicted_labels)
    return recall, precision, label_no_zero,nofLabsCaptured

def findBatchMetrics(batchScores,batchY2):
    threshold = 0.5
    scores = np.ndarray.tolist(batchScores)
    predicted_labelsB = []
    predicted_valuesB = []
    pred_labels_acc = []
    true_labelsB = []
    nofLabsCapturedG = 0
    cur_rec, cur_pre, cur_F = 0.0, 0.0, 0.0
    for scoreTemp in batchScores:
        score = np.ndarray.tolist(scoreTemp)
        count = 0
        index_list = []
        ind_list2 = []
        value_list = []
        for index, predict_value in enumerate(score):
            if predict_value > threshold:
                index_list.append(index)
                value_list.append(predict_value)
                count += 1
        if count == 0:
            index_list.append(score.index(max(score)))
            value_list.append(max(score))
        ind_list2.append(score.index(max(score)))
        predicted_labelsB.append(index_list)
        predicted_valuesB.append(value_list)
        pred_labels_acc.append(ind_list2)
    #print("predicted_labelsB : ",predicted_labelsB)
    
    for index, predicted_labels_list in enumerate(predicted_labelsB):
        rec_inc, pre_inc, true_labels_list,nofLabsCaptured = cal_metric(predicted_labels_list, batchY2[index])
        nofLabsCapturedG += nofLabsCaptured
        cur_rec, cur_pre = cur_rec + rec_inc, cur_pre + pre_inc
        true_labelsB.append(true_labels_list)
    #print("len(batchY2) :",len(batchY2))
    cur_rec = cur_rec / len(batchY2)
    cur_pre = cur_pre / len(batchY2)
    return cur_rec,cur_pre,predicted_labelsB,true_labelsB,nofLabsCapturedG,pred_labels_acc

from sklearn.metrics import f1_score
testBatchSize = 32
nofTestbBatches = int(test_df.shape[0]/testBatchSize)
print("nofTestbBatches :",nofTestbBatches)
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    train_df = train_df.sample(frac=1, replace=False)       

    test_counter, test_loss, test_rec, test_pre, test_F = 0, 0.0, 0.0, 0.0, 0.0
    nofLabsCapturedG = 0
    nofUniSamplesG = 0
    nofPredsG = 0
    
    saver1 = tf.train.import_meta_graph("L2_multiLabel_bestModel_elmo_withSW/bestModel_biAtt.meta")
    graph = tf.get_default_graph()
    X = graph.get_tensor_by_name('X:0')
    Y2 = graph.get_tensor_by_name('Y2:0')
    sensListT = graph.get_tensor_by_name('sensListT:0')
    lengthsVec = graph.get_tensor_by_name('lengthsVec:0')
    dropoutLstmLayer = graph.get_tensor_by_name('Placeholder:0')
    dropoutPenultimate = graph.get_tensor_by_name('Placeholder_1:0')

    scores = graph.get_tensor_by_name('scores:0')
    saver1.restore(sess,tf.train.latest_checkpoint('L2_multiLabel_bestModel_elmo_withSW/'))
    
    ytrue_global_oh = []
    ypred_global_oh = []

    acc_cor_predsG = 0
    acc_nof_predsG = 0
    nof_exactMatchesG_min2 = 0
    nof_samplesG_min2Lab = 0
    for testBatchIter in range(nofTestbBatches): #nofTestbBatches
        batchDataDf = test_df[testBatchIter*testBatchSize : testBatchIter*testBatchSize+testBatchSize]
        batchX,batchY2_test,lengthsVecArr,rawSensList = getEncodedData(batchDataDf,vocabDict,L2LabelsDict,AxiomWVsDict)
        scores_batch_test = sess.run(scores, feed_dict={X: batchX, sensListT:rawSensList, Y2:batchY2_test,lengthsVec:lengthsVecArr, dropoutLstmLayer: 0, dropoutPenultimate: 0})
        cur_rec,cur_pre,predicted_labelsB,true_labelsB,nofLabsCapturedB,pred_label_acc = findBatchMetrics(scores_batch_test,batchY2_test)
        for pred_label_list in predicted_labelsB:
             nofPredsG += len(pred_label_list)
             curSampleOHLabelPred = np.zeros(shape=[nofClassesInL2])
             for pred_label in pred_label_list:
                 curSampleOHLabelPred[pred_label] = 1
             ypred_global_oh.append(curSampleOHLabelPred)
        for true_label_oh in batchY2_test:
             ytrue_global_oh.append(true_label_oh)
        for ind5,true_label_list in enumerate(true_labelsB):
             nofUniSamplesG += len(true_label_list)
             pred_label_list = predicted_labelsB[ind5]
             count5 = 0
             for cur_true_label in true_label_list:
                 if cur_true_label in pred_label_list:
                       count5 += 1
             if len(true_label_list)>1:
                nof_samplesG_min2Lab += 1
                if count5 == len(true_label_list) and len(true_label_list) == len(pred_label_list):
                      nof_exactMatchesG_min2 += 1
        nofLabsCapturedG += nofLabsCapturedB
        #printPredictions(batchDataDf,predicted_labelsB,true_labelsB,revLabelsDict)
        test_rec, test_pre = test_rec + cur_rec, test_pre + cur_pre
        test_counter += 1
        acc_nof_predsG += len(pred_label_acc)
        for ind4,pred_label_list in enumerate(pred_label_acc):
              if pred_label_list[0] in true_labelsB[ind4]:
                   acc_cor_predsG += 1
    test_rec = float(test_rec / test_counter)
    test_pre = float(test_pre / test_counter)
    
    print("exact match acc : ",float(nof_exactMatchesG_min2)/nof_samplesG_min2Lab,"..nof_exactMatchesG : ",nof_exactMatchesG_min2, "..nof_samplesG_min2Lab :",nof_samplesG_min2Lab)
    print("acc ---- acc_cor_predsG :",acc_cor_predsG,"..acc_nof_predsG :",acc_nof_predsG,"..acc :",float(acc_cor_predsG)/acc_nof_predsG)
    #print("F1 score sklearn macro :",f1_score(ytrue_global_oh, ypred_global_oh, average='macro'))
    #print("F1 score sklearn micro :",f1_score(ytrue_global_oh, ypred_global_oh, average='micro'))
    #print("F1 score sklearn weighted :",f1_score(ytrue_global_oh, ypred_global_oh, average='weighted'))
    print("nof captured samples :",nofLabsCapturedG,"..nofUniSamples : ",nofUniSamplesG,"nofPredsGlob :",nofPredsG)
    test_F = cal_F(test_rec, test_pre)
    print( "Test F1 : ",test_F, "Test Recall : ",test_rec, "Test precision : ",test_pre, "... t_counter :",test_counter, "....",str(datetime.now()) ," ....")
